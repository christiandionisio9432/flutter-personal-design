import 'package:flutter/material.dart';

class BasicDesignScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // Imagen
          Image(image: AssetImage('assets/landscape2.jpg')),

          // Título
          Title(),

          //Botones
          ButtonSection(),

          // Descripcion
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
                'Nostrud sit officia culpa in. Eiusmod magna dolor voluptate id. Cillum magna veniam pariatur commodo ea labore amet dolore ad amet do elit occaecat. Mollit laborum do mollit reprehenderit irure dolore aute qui enim sit eu sit sunt. Eiusmod aute fugiat cillum nostrud quis fugiat consectetur nisi occaecat ex ea in. Ullamco Lorem sunt tempor occaecat fugiat ex cillum est culpa eiusmod consequat commodo ea ullamco. Deserunt cillum ipsum eiusmod do commodo officia magna ex ea duis nulla.'),
          )
        ],
      ),
    );
  }
}

class ButtonSection extends StatelessWidget {
  const ButtonSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ButtonBody(Icons.call, 'CALL'),
          ButtonBody(Icons.send_to_mobile_sharp, 'ROUTE'),
          ButtonBody(Icons.share, 'SHARE'),
        ],
      ),
    );
  }
}

class ButtonBody extends StatelessWidget {
  final IconData icon;
  final String text;

  const ButtonBody(this.icon, this.text);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(this.icon, color: Colors.blue),
        Divider(height: 5),
        Text(this.text, style: TextStyle(color: Colors.blue)),
      ],
    );
  }
}

class Title extends StatelessWidget {
  const Title({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Oeschinen Lake Campground',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text('Kandersteg, Switzerland',
                  style: TextStyle(color: Colors.black45)),
            ],
          ),

          // Se expande todo lo que pueda: derecha izquierda
          Expanded(child: Container()),

          Icon(Icons.star, color: Colors.red),
          Text('41'),
        ],
      ),
    );
  }
}
